'use strict';

angular.module('pokerface', [

  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'chart.js',
])
    .config(function ($routeProvider, $locationProvider) {
      $routeProvider
          .otherwise({
            redirectTo: '/'
          });
    });
