'use strict';

angular.module('pokerface')
    .directive('opponentsCard', function ($timeout, $rootScope) {
      return {
        templateUrl: 'app/opponentsCard/opponentsCard.html',
        restrict: 'EA',
        replace: true,
        scope: {
          //Make the directive aware of which face's features it needs to display
          index: '='
        },
        link: function (scope, element, attrs) {

          // Randomly add a data point every 500ms
          var random = new TimeSeries();
          setInterval(function () {
            //random.append(new Date().getTime(), Math.random());
          }, 500);

          //Set up the continuous
          var chart = new SmoothieChart({
            responsive: true,
            grid: {fillStyle: '#ffffff', sharpLines: true, millisPerLine: 4000, borderVisible: false},
            maxValue: 1,
            minValue: 0
          });
          chart.addTimeSeries(random, {
            lineWidth: 2,
            strokeStyle: '#95b8ca',
            fillStyle: 'rgba(149,184,209,0.30)'
          });
          chart.streamTo(element[0].querySelector('#chart'), 500);


          scope.options = {
            scales: {
              yAxes: [{
                display: true,
                ticks: {
                  beginAtZero: true,   // minimum value will be 0.
                  min: 0,
                  max: 1,
                }
              }]
            }
          };
          scope.labels = ["sadness", "anger", "disgust", "surprise", "contempt", "happiness", "neutral"];
          scope.data = [1, 1, 1, 1, 1, 1, 1,];


          $rootScope.$on('faces', function (events, faces) {
            if (faces.length > scope.index) {
              scope.recognized = true;
              //Update the data for the radar plot
              var min = 1;
              _.forEach(scope.labels, function (label) {
                    var value = faces[scope.index].result.Results.output1[0][label];
                    if (value < min && value > 0) min = value;
                  }
              );
              _.forEach(scope.labels, function (label, index) {
                var value = Math.log((faces[scope.index].result.Results.output1[0][label] * 10 / min));
                scope.data[index] = value;
              });

              //Push the Data to the line plot
              var value = faces[scope.index].result.Results.output1[0]["Scored Probabilities"];
              random.append(new Date().getTime(), value);

              //Set the Bluffing-value
              scope.isBluffing = (faces[scope.index].result.Results.output1[0]["Scored Labels"] == '1');
            } else {
              scope.recognized = false;
            }

            $timeout(function () {
              scope.$digest();
            }, 0);
          })
          ;


          /*
           scope.data = [];
           scope.labels = [];

           scope.randomize = function () {
           _(4).times(function (index) {
           scope.labels[index] = index;
           scope.data[index] = Math.random();
           });

           $timeout(function () {
           scope.$digest();
           }, 0);
           };

           scope.randomize();

           scope.push = function () {

           scope.data[0] = scope.data[1];
           scope.data[1] = scope.data[2];
           scope.data[2] = scope.data[3];
           scope.data[3] = Math.random();

           $timeout(function () {
           scope.$digest();
           }, 0);

           }
           */

        }
      };
    });
