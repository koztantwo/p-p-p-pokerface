'use strict';

angular.module('pokerface')
    .directive('opponentsStream', function ($http, $rootScope) {
      return {
        templateUrl: 'app/opponentsStream/opponentsStream.html',
        restrict: 'EA',
        link: function (scope, element, attrs) {

          $rootScope.$on('faces', function (events, faces) {

            var video = document.getElementById('video');
            var canvas = document.getElementById('videoCanvas');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            canvas.style.top = video.offsetTop + 'px';
            canvas.style.left = video.offsetLeft + 'px';
            var context = canvas.getContext('2d');
            context.lineWidth = '3';
            context.strokeStyle = 'red';

            _.forEach(faces, function (face) {
              console.log(face);
              var rectangle = face.faceRectangle;
              context.rect(rectangle.left, rectangle.top, rectangle.width, rectangle.height);
              if (face.result && face.result.Results.output1[0]['Scored Labels'] == '1') {
                var trollImg = document.getElementById("trollImg");
                var trollLeft = rectangle.left + rectangle.width - 70;
                var trollTop = rectangle.top + rectangle.height - 64;
                context.drawImage(trollImg, trollLeft, trollTop, 70, 64);
              }
              //TODO Also sort the faces by position so it doesn't jump around
            });
            context.stroke();
          });

          // Grab elements, create settings, etc.
          var video = document.getElementById('video');
          // Get access to the camera!
          if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {

            //by adding facingMode: "user" to the options we could also get the front camera for self-analysis
            navigator.mediaDevices.getUserMedia({video: true}).then(function (stream) {
              video.src = window.URL.createObjectURL(stream);
              video.play();
            });
          }

          scope.sendImage = function () {
            //Measure Time
            var start = Date.now();

            //Create a canvas the image is rendered on
            var canvas = document.createElement("CANVAS");
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            canvas.getContext('2d').drawImage(video, 0, 0);
            var dataUri = canvas.toDataURL('img/jpeg');
            var data = dataUri.split(',')[1];
            var mimeType = dataUri.split(';')[0].slice(5);

            console.log(mimeType);

            var bytes = window.atob(data);
            var buf = new ArrayBuffer(bytes.length);
            var arr = new Uint8Array(buf);

            for (var i = 0; i < bytes.length; i++) {
              arr[i] = bytes.charCodeAt(i);
            }

            var blob = new Blob([arr], {type: mimeType});

            var config = {headers: {'Content-Type': undefined}};

            var sending = Date.now();
            $http.post('/api/upload', blob, config).then(function (res) {
              console.log(
                  "sending snap of size " + blob.size +
                  "to server took " + (Date.now() - start) +
                  ", " + (Date.now() - sending) + "of which was on the server side");

              //We got new data, which needs to be broadcasted so the rectangles on top of the faces and the emotion/bluffing data can be updated
              $rootScope.$broadcast('faces', res.data)

            }, console.error)
          };

          var interval = setInterval(function () {
            scope.sendImage();
          }, 1000);
        }
      };
    });
