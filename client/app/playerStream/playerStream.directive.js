'use strict';

angular.module('pokerface')
  .directive('playerStream', function () {
    return {
      templateUrl: 'app/playerStream/playerStream.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
      }
    };
  });
