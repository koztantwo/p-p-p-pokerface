'use strict';

angular.module('pokerface')
  .directive('playerCard', function () {
    return {
      templateUrl: 'app/playerCard/playerCard.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
      }
    };
  });
