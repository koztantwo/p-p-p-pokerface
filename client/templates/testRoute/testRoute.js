'use strict';

angular.module('pokerface')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/dashboard', {
        templateUrl: 'app/testRoute/testRoute.html',
        controller: 'TestRouteCtrl'
      });
  });
