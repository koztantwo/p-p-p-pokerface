import threading
import time

import _thread
from flask import Flask, request, jsonify, render_template, send_from_directory
import os, json

from pls_refactor.processImage import processImage
from query_endpoint import query_endpoint

app = Flask(__name__, template_folder="./client")

frame_counter = 0


@app.route('/')
def index():
    return send_from_directory('./client', 'index.html')


@app.route('/<path:path>')
def send(path):
    print(path)
    return send_from_directory('./client', path)


@app.route('/api/upload', methods=['GET', 'POST'])
def upload():
    image = request.data
    result_image = processImage(image)
    result = []

    if result_image is None:
        print("backend API request failed")
        return jsonify([])
    if len(result_image) == 0:
        return jsonify([])
    for face in result_image:
        thread = threading.Thread(target=paralellFaceRequest, args=(face, result))
        thread.start()
        thread.join()

    result.sort(key=lambda x: (x['faceRectangle']['left'], x['faceRectangle']['top']), reverse=True)

    response = app.response_class(
        response=json.dumps(result),
        status=200,
        mimetype='application/json'
    )
    return response


def paralellFaceRequest(face, result):
    output = {}
    output["faceRectangle"] = face["faceRectangle"]

    learning_input = {}
    learning_input["input1"] = [face["scores"]]
    learning_input["input1"][0]["bluff"] = 0
    learning_input["input1"][0]["id"] = 0

    # learning_input = json.load(learning_input)

    result_learning = query_endpoint(learning_input)

    output["result"] = json.loads(result_learning.decode())
    result.append(output)


@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r


if __name__ == '__main__':
    app.run(threaded=True)
