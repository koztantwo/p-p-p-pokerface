import json
import urllib
from urllib.request import urlopen, Request



def query_endpoint(input):
    data =  {
        "Inputs": {
            "input1":
                [
                    {
                        'bluff': "0",
                        'anger': "0",
                        'contempt': "0",
                        'disgust': "0",
                        'fear': "0",
                        'happiness': "0",
                        'neutral': "0",
                        'sadness': "0",
                        'surprise': "0",
                    }
                ],
        },
        "GlobalParameters": {
            'Path to container, directory or blob': "",
        }
    }

    data["Inputs"] = input

    body = str.encode(json.dumps(data))
    url = 'https://europewest.services.azureml.net/workspaces/2c646b254ec64a6e806b5338f7281c73/services/307c93ae9efd45a4bea4c2675a44b389/execute?api-version=2.0&format=swagger'
    headers = {'Content-Type': 'application/json', 'Authorization': (
    'Bearer ' + '9i4f49PxRzwVKB0PgNMHgFm12+uQ/qpucpvb0mjQvHwJrwPkhBlBZJDVbZ2h2IFUOxLik6ZgLYMgwWIf1DcriQ==')}
    # alternate endpoint (from poker videos)
    #url = 'https://europewest.services.azureml.net/workspaces/2c646b254ec64a6e806b5338f7281c73/services/0eef6ecc430b40c09b0e586be4eca1ca/execute?api-version=2.0&format=swagger'
    #headers = {'Content-Type': 'application/json', 'Authorization': (
    #'Bearer ' + '/V2wezRiOfbfRZ1ar0DgZY7lazgUQIGZffBGMWxxLTXEGotTu0QzLIZsQrmFcf4SzTacfF25nJ1slgjuCwf5kw==')}
    pi_key = 'abc123'  # Replace this with the API key for the web service

    req = Request(url, body, headers)

    try:
        response = urlopen(req)


        result = response.read()
        return result
    except urllib.HTTPError as error:
        print("The request failed with status code: " + str(error.code))

        # Print the headers - they include the requert ID and the timestamp, which are useful for debugging the failure
        print(error.info())
        print(json.loads(error.read()))


query_endpoint({
            "input1":
                [
                    {
                        'bluff': "0",
                        'anger': "0",
                        'contempt': "0",
                        'disgust': "0",
                        'fear': "0",
                        'happiness': "0",
                        'neutral': "0",
                        'sadness': "0",
                        'surprise': "0",
                        'id':"0"
                    }
                ],
        })