import csv
import glob
import http.client
import json
import os
import urllib

import time

import _thread

from cv2.cv2 import imread, IMREAD_COLOR

headers = {
    # Request headers
    'Content-Type': 'application/octet-stream',
    'Ocp-Apim-Subscription-Key': '54d5527d396d4e0c8d4006d10f7a57c9',
}

params = urllib.parse.urlencode({

})



def processImage(image):
    try:
        #start = time.time()
        conn = http.client.HTTPSConnection('westus.api.cognitive.microsoft.com')
        conn.request("POST", "/emotion/v1.0/recognize?%s" % params, image, headers)
        response = conn.getresponse()
        data = response.read()
        j = json.loads(data.decode('utf-8'))
        conn.close()
        #print("thread ended: "+str(time.time()-start))
        return j
    except Exception as e:
        print("[Errno {0}] {1}".format(e.errno, e.strerror))
        print("thread ended")


# print(cv2.__version__)
# dir = os.path.dirname(__file__)
# file = open(os.path.join(dir, '../resources/video2.mp4'), 'rb').read()
# vidcap = cv2.VideoCapture(os.path.join(dir, '../resources/video.mp4'))
# success, image = vidcap.read()


def save_to_csv(dict):
    with open("database.csv", "a") as file:
        w = csv.DictWriter(file, fieldnames=["anger", "contempt", "disgust", "fear", "happiness", "neutral", "sadness",
                                             "surprise", "bluff", "id"])
        w.writeheader()

        for i in dict:
            w.writerow(dict[i])


def processDirectory():
    dir = os.path.dirname(__file__)

    dict = {}
    # files = os.listdir("/Users/jonaneumeier/Downloads/bluff1/*.png")
    i = 0

    for f in glob.iglob('/Users/jonaneumeier/Downloads/bluff1/*.png'):
        try:
            image = open(f, 'rb').read()
            result = processImage(image)

            print(i)

            dict[i] = result[0]["scores"]
            dict[i]["id"] = i
            dict[i]["bluff"] = 0
            i += 1
        except Exception:
            pass

    print(dict)
    save_to_csv(dict)


if __name__ == '__main__':
    # processDirectory()
    dir = os.path.dirname(__file__)
    image = imread(os.path.join(dir, '../resources/_LEO0377.jpg'), IMREAD_COLOR)

    print("start all threads")
    for i in range(20):
        _thread.start_new_thread(processImage, (image,))

    print("all threads started")


    c = input("Eingabe.")





# id, bluff, anger, contempt, disgust, fear, happiness, neutral, sadness, surprise
