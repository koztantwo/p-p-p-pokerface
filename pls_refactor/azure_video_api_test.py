import http.client
import os
import urllib.error

headers = {
    # Request headers
    'Content-Type': 'application/octet-stream',
    'Ocp-Apim-Subscription-Key': '54d5527d396d4e0c8d4006d10f7a57c9',
}

params = urllib.parse.urlencode({
    # Request parameters
    'outputStyle': 'aggregate',
})


try:
    dir = os.path.dirname(__file__)
    file = open(os.path.join(dir, '../resources/video.mp4'), 'rb').read()
    conn = http.client.HTTPSConnection('westus.api.cognitive.microsoft.com')
    conn.request("POST", "/emotion/v1.0/recognizeinvideo?%s" % params, file, headers)
    response = conn.getresponse()
    print(str(response.getheader('Operation-Location')))
    data = response.read()
    print(data)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))
