import http.client
import json
import os
import urllib.error

headers = {
    # Request headers
    'Ocp-Apim-Subscription-Key': '54d5527d396d4e0c8d4006d10f7a57c9',
}

params = urllib.parse.urlencode({
    # Request parameters
    'outputStyle': 'aggregate',
})

try:
    conn = http.client.HTTPSConnection('westus.api.cognitive.microsoft.com')
    conn.request("GET", "/emotion/v1.0/operations/a064bc17-a255-4abc-931a-3e49d38b96a6", '', headers)
    response = conn.getresponse()
    data = response.read()
    #print(str(json.loads(data, 'utf-8')))
    j = json.loads(data.decode('utf-8'))
    processing_result = j['processingResult']
    print(processing_result)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))
